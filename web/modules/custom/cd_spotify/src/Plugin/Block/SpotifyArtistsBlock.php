<?php

namespace Drupal\cd_spotify\Plugin\Block;

// Arbitrary selection of artists from my saved albums
define('ARTISTS_COLLECTION',
  [
    '1J8H4wufuy6qxfdRbJr5xG',
    '6AdRO941ZEDh4GHcCUdEs4',
    '6cGVZq9WhCCRkTnn4cJYOg',
    '5E3H2KyR31E2Dj3K6vIUe9',
    '6YoOXawnD9vM6t01jB4GtU',
    '2FEWIj8qxL0aJKlH5QoOnt',
    '5gqhueRUZEa7VDnQt4HODp',
    '7IdPmzvB3PugXieZE9vS4S',
    '4CvTDPKA6W06DRfBnZKrau',
    '1qiwaJwjKod5WhcYZ76O1B',
    '34gLicNdz493863yZTanvC',
    '0wz0jO9anccPzH04N7FLBH',
    '05lIUgmmsmTX2N9dCKc8rC',
    '78vuz6ahePQXzGtqcU1SUq',
    '0ZUyFEafMwocvApBjTXvdo',
    '3i7IUsb5VsiJAKqX2Md9Fc',
    '5cHmjJA9Lo0ga8s8QdxEzs',
    '4UXJsSlnKd7ltsrHebV79Q',
    '6xrCU6zdcSTsG2hLrojpmI',
    '6zvul52xwTWzilBZl6BUbT',
  ]
);

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides the Artists listing block.
 *
 * @Block(
 *   id = "cd_spotify_spotify_artists",
 *   admin_label = @Translation("Spotify Artists"),
 *   category = @Translation("cd_spotify")
 * )
 */
class SpotifyArtistsBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $service = \Drupal::service('cd_spotify.spotify_api_link');

    $config = $this->getConfiguration();

    if (!empty($config['artists_count'])) {

      $artists_count = $config['artists_count'];
      $artists_max = array_rand(array_flip(ARTISTS_COLLECTION), $artists_count);
      $artists = $service->getSpotifyArtists($artists_max);

      $artist_displayed = [];

      foreach ($artists as $key => $artist) {
        $url = Url::fromRoute('cd_spotify.content', array('artist_id' => $artist->id));
        $internal_link = Link::fromTextAndUrl(t($artist->name), $url);
        $artist_displayed[] = [
          'name' => $artist->name,
          'link' => $internal_link
        ];
      }

      array_multisort(array_column($artist_displayed, 'name'), SORT_ASC, $artist_displayed);

    }

    return array(
      '#theme' => 'block__spotify_artists',
      '#artists_count' => $artists_count,
      '#artist_displayed' => $artist_displayed
    );

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $options = [];

    for ($i = 1; $i <= 20; $i++) {
      $options[$i] = $i;
    }

    $form['artists_count'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('How many Artists would you like to be displayed'),
      '#options' => $options,
      '#required' => true,
      '#default_value' => isset($config['artists_count']) ? $config['artists_count'] : 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['artists_count'] = $values['artists_count'];
  }

}

