<?php

namespace Drupal\cd_spotify;

use SpotifyWebAPI;

/**
 * SpotifyApiLink service.
 */
class SpotifyApiLink {

  /**
   * Retrieve multiple Artists infos.
   *
   * @param $ids
   *
   * @return mixed
   */
  public function getSpotifyArtists($ids) {
    $api = $this->spotifyConnector();
    $artists = $api->getArtists($ids)->artists;
    return $artists;
  }

  /**
   * Retrieve a specific Artist details.
   *
   * @param $id
   *
   * @return mixed
   */
  public function getSpotifyArtist($id) {
    $api = $this->spotifyConnector();
    $artist = $api->getArtist($id);
    return $artist;
  }

  /**
   * Connect to Spotify API.
   *
   * @param $id
   *
   * @return mixed
   */
  public function spotifyConnector() {

    // Would ideally store clientId and clientSecret in an .ENV file
    $session = new SpotifyWebAPI\Session(
      'cd858c991d4e46e08455c1a84b23f644',
      '626f735221f140e1b31fc9ff09afc583'
    );

    $session->requestCredentialsToken();
    $accessToken = $session->getAccessToken();

    $api = new SpotifyWebAPI\SpotifyWebAPI();
    $api->setAccessToken($accessToken);

    return $api;

  }



}
