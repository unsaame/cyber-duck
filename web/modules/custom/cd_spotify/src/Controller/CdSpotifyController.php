<?php

namespace Drupal\cd_spotify\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Returns responses for cd_spotify routes.
 */
class CdSpotifyController extends ControllerBase {

  /**
   * Builds the response.
   *
   * @param $artist_id
   *
   * @return mixed
   */
  public function build($artist_id) {

    $service = \Drupal::service('cd_spotify.spotify_api_link');

    $artist = $service->getSpotifyArtist($artist_id);
//    $artist->external_urls->spotify

    $url = Url::fromUri($artist->external_urls->spotify);
    $play_link = Link::fromTextAndUrl(t('Listen now'), $url);

    asort($artist->genres);

    return [
      '#theme' => 'page__spotify_artist',
      '#title' => $artist->name,
      '#artist_pic' => $artist->images[0]->url,
      '#artist_genres' => $artist->genres,
      '#artist_play' => $play_link,
    ];

  }

}
